<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\TransactionResource;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return TransactionResource::collection(
            Transaction::simplePaginate($request->input('page') ?? 10)
        )->additional([
            'meta' => [
                'message' => 'transactions loaded',
                'success' => true
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required|string',
            'amount' => 'required|numeric',
            'type' => 'required|string:in()',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toArray(), 422);
        }

        $transaction = new Transaction();
        $transaction->description = $request->description;
        $transaction->amount = $request->amount;
        $transaction->type = $request->type;
        $transaction->save();

        return (new TransactionResource($transaction))
            ->additional([
                'meta' => [
                    'success' => true,
                    'message' => "transaction created"
                ]
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        return (new TransactionResource($transaction))
            ->additional([
                'meta' => [
                    'success' => true,
                    'message' => "transaction found"
                ]
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required|string',
            'amount' => 'required|numeric',
            'type' => 'required|string:in()',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toArray(), 422);
        }

        $transaction->description = $request->description;
        $transaction->amount = $request->amount;
        $transaction->type = $request->type;
        $transaction->save();

        return (new TransactionResource($transaction))
            ->additional([
                'meta' => [
                    'success' => true,
                    'message' => "transaction updated"
                ]
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        $transaction->delete();

        return response()->json('transaction has been deleted', 204);
    }
}
