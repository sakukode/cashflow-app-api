<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $amounts = collect([10, 25, 50, 75, 100]);
        $types = collect(['income', 'expense']);

        return [
            'description' => $this->faker->sentence,
            'amount' => $amounts->random(),
            'type' => $types->random()
        ];
    }
}
