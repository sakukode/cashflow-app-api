<?php

namespace Tests\Feature;

use App\Models\Transaction;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TransactionTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_can_get_transaction_list()
    {
        Transaction::factory()->count(25)->create();

        $response = $this->get(route('transactions.index'));

        $response->assertSuccessful();

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'description',
                    'amount',
                    'type',
                ]
            ],
            'links' => [
                'first',
                'last',
                'prev',
                'next',
            ],
            'meta' => [
                "current_page",
                "from",
                "path",
                "per_page",
                "to",
                "success",
                "message",
            ]
        ]);
    }

    public function test_can_get_transaction_detail()
    {
        $transaction = Transaction::factory()->create();

        $response = $this->get(route('transactions.show', $transaction->id));

        $response->assertSuccessful();

        $response->assertJson([
            'data' => [
                'id' => $transaction->id,
                'description' => $transaction->description,
                'amount' => $transaction->amount,
                'type' => $transaction->type,
            ],
            'meta' => [
                'success' => true
            ]
        ]);
    }

    public function test_can_create_a_new_transaction()
    {
        $transaction = Transaction::factory()->make();

        $response = $this->post(route('transactions.store'), [
            'description' => $transaction->description,
            'amount' => $transaction->amount,
            'type' => $transaction->type
        ]);

        $response->assertSuccessful();

        $this->assertDatabaseHas('transactions', [
            'description' => $transaction->description,
            'amount' => $transaction->amount,
            'type' => $transaction->type
        ]);
    }

    public function test_can_update_an_existing_transaction()
    {
        $transaction = Transaction::factory()->create();

        $response = $this->put(route('transactions.update', $transaction->id), [
            'description' => $description = $this->faker->sentence,
            'amount' => $amount = 99,
            'type' => $type = 'income'
        ]);

        $response->assertSuccessful();
     
        $this->assertDatabaseHas('transactions', [
            'description' => $description,
            'amount' => $amount,
            'type' => $type
        ]);
    }

    public function test_can_delete_an_existing_transaction()
    {
        $transaction = Transaction::factory()->create();

        $response = $this->delete(route('transactions.destroy', $transaction->id));

        $response->assertSuccessful();

        $this->assertDatabaseMissing('transactions', [
            'id' => $transaction->id
        ]);
    }
}
